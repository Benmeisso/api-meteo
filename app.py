from dotenv import load_dotenv

import sqlite3
import requests
import os
import web
import json

# Loading file

load_dotenv()
Apikey = os.getenv('Apikey')
apiurl = os.getenv('url')

# Generating routes

urls = (
    '/', 'home',
    '/zipcode=([0-9]{5})', 'cp',
)
app = web.application(urls, globals())

# Database

connection = sqlite3.connect('weather.db')
"""
cur = connection.cursor()
cur.execute('''CREATE TABLE location
               (weather text, temp text, mintemp text, maxtemp text)''')
cur.close()
"""
# Class

class home:
    def GET(self):
        web.header('content-type', "application/json")
        web.header('charset', 'utf-8')
        return "Bienvenue, veuilliez saisir un code postal français ."

class cp:
    def GET(self, zip):
        if not zip:
            return "Veuilliez saisir une adresse postale valide en france."
        constk = 273.15

        web.header('content-type', 'application/json')
        web.header('charset', 'utf-8')

        response = requests.get(apiurl + zip + ",FR&appid=" + Apikey)
        jsonresponse = response.json()

        weather = str(jsonresponse['weather'][0]['main'])
        actTemp = str(round(float(jsonresponse['main']['temp'] - constk), 2))
        minTemp = str(round(float(jsonresponse['main']['temp_min'] - constk), 2))
        maxtemp = str(round(float(jsonresponse['main']['temp_max'] - constk), 2))
        cursor = connection.cursor()
        sql = "INSERT INTO Location VALUES('{weather}','{temp}', '{mintemp}', '{maxtemp}')".format(weather=weather, temp=actTemp, mintemp=minTemp, maxtemp=maxtemp)
        cursor.execute(sql)
        cursor.close()

        connection.commit()

        mydict = {
            'weather': weather,
            'temp': actTemp,
            'mintemp': minTemp,
            'maxtemp': maxtemp,
        }

        return json.dumps(mydict)

if __name__ == "__main__":
    app.run()
