# Lancement de l'application

`python app.py`

# Utiliser sa propre clé APi

Renseignez le fichier .env avec votre propre clé api tel que défini dans ".env.sample".

# Tester l'application

Accédez à l'application via l'url : `http://localhost:8080`
Vous pouvez également accéder à des sous routes telles que :

 `http://localhost:8080//zipcode=([0-9]{5})`

# Bdd sqlite3

-Lors de la première exécution de l'api décommenter les lignes 26 à 31.

-Ensuite les recommenter.

-Puis utiliser l'url afin d'obtenir les infos de météo.

-Enfin copier ce qui suit dans la console python afin de visualiser le stockage en bdd.

import sqlite3
connection = sqlite3.connect('weather.db')
cur = connection.cursor()
for row in cur.execute('SELECT * FROM location ORDER BY weather'):
        print(row)
